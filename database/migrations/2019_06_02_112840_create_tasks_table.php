<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('related_module')->nullable();
            $table->bigInteger('related_id')->nullable();
            $table->string('status');
            $table->string('priority');
            $table->string('subject');
            $table->text('description')->nullable();
            $table->date('due_date');
            $table->dateTime('closed_time')->nullable();
            $table->dateTime('notification_time')->nullable();
            $table->boolean('notified')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
