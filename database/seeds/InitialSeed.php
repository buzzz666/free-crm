<?php

use Illuminate\Database\Seeder;

class InitialSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $adminRole = \App\Models\Role::create([
            'label' => 'Admin',
            'slug' => 'admin',
            'description' => 'Administrative role. Has full access rights.'
        ]);
        $managerRole = \App\Models\Role::create([
            'label' => 'Manager',
            'slug' => 'manager',
            'description' => 'Manager role. Has management access rights.'
        ]);
        $supervisorRole = \App\Models\Role::create([
            'label' => 'Supervisor',
            'slug' => 'supervisor',
            'description' => 'Supervisor role. Has full access to managers records, but cant access settings.'
        ]);
        $admin = \App\Models\User::create([
            'name' => 'Admin',
            'email' => 'admin@free-crm.pp.ua',
            'email_verified_at' => now(),
            'password' => bcrypt('password'),
            'remember_token' => Illuminate\Support\Str::random(10),
            'role_id' => $adminRole->id,
            'api_token' => \Illuminate\Support\Str::random(60),
            'superadmin' => 1
        ]);
        $manager = \App\Models\User::create([
            'name' => 'Manager',
            'email' => 'manager@free-crm.pp.ua',
            'email_verified_at' => now(),
            'password' => bcrypt('password'),
            'remember_token' => Illuminate\Support\Str::random(10),
            'role_id' => $managerRole->id,
            'api_token' => \Illuminate\Support\Str::random(60),
        ]);
        $supervisor = \App\Models\User::create([
            'name' => 'Supervisor',
            'email' => 'supervisor@free-crm.pp.ua',
            'email_verified_at' => now(),
            'password' => bcrypt('password'),
            'remember_token' => Illuminate\Support\Str::random(10),
            'role_id' => $supervisorRole->id,
            'api_token' => \Illuminate\Support\Str::random(60),
        ]);

        for ($i = 0; $i < 100; $i++) {
            \App\Models\Contact::create([
                'user_id' => rand(1, 3),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'phone' => $faker->phoneNumber,
                'mobile' => $faker->phoneNumber,
                'skype' => \Illuminate\Support\Str::random(10),
                'email' => $faker->email,
                'other_email' => $faker->email,
                'website' => $faker->url,
                'street' => $faker->streetName,
                'city' => $faker->city,
                'state' => $faker->address,
                'zip' => $faker->postcode,
                'country' => $faker->country,
                'description' => $faker->text,
                'utm_campaign' => $faker->text(10),
                'utm_medium' => $faker->text(10),
                'utm_term' => $faker->text(10),
                'utm_content' => $faker->text(10),
                'utm_source' => $faker->text(10),
                'gclid' => \Illuminate\Support\Str::random(30),
            ]);

            $stages = [
                'New',
                'First Contact',
                'Demo/Presentation',
                'Needs Analysis',
                'Proposal/Price Quote',
                'Negotiation/Review',
                'Contract',
                'Execution',
                'Closed Won',
                'Closed Lost',
            ];

            $priorities = [
                'Highest',
                'High',
                'Medium',
                'Low',
                'Lowest',
                'Highest',
            ];

            \App\Models\Deal::create([
                'user_id' => rand(1, 3),
                'contact_id' => rand(1,100),
                'name' => $faker->text(20),
                'stage' => $faker->randomElement($stages),
                'amount' => $faker->randomFloat(2),
                'currency' => $faker->randomElement(['USD', 'UAH', 'EUR']),
                'product' => $faker->text(20),
                'company' => $faker->company,
                'date' => $faker->date('Y-m-d'),
                'priority' => $faker->randomElement($priorities),
                'source' => $faker->text(30),
                'description' => $faker->text(200),
            ]);

            \App\Models\Lead::create([
                'user_id' => rand(1, 3),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'phone' => $faker->phoneNumber,
                'mobile' => $faker->phoneNumber,
                'skype' => \Illuminate\Support\Str::random(10),
                'email' => $faker->email,
                'other_email' => $faker->email,
                'website' => $faker->url,
                'lead_source' => $faker->realText(100),
                'street' => $faker->streetName,
                'city' => $faker->city,
                'state' => $faker->address,
                'zip' => $faker->postcode,
                'country' => $faker->country,
                'description' => $faker->text,
                'utm_campaign' => $faker->text(10),
                'utm_medium' => $faker->text(10),
                'utm_term' => $faker->text(10),
                'utm_content' => $faker->text(10),
                'utm_source' => $faker->text(10),
                'gclid' => \Illuminate\Support\Str::random(30),
            ]);

            $modules = [
                'Lead',
                'Contact',
                'Deal',
            ];

            $statuses = [
                'New',
                'Started',
                'Finished',
                'Failed',
                'Declined',
            ];

            \App\Models\Task::create([
                'user_id' => rand(1, 3),
                'related_module' => $faker->randomElement($modules),
                'related_id' => rand(1,100),
                'status' => $faker->randomElement($statuses),
                'priority' => $faker->randomElement($priorities),
                'subject' => $faker->text(20),
                'due_date' => $faker->date('Y-m-d'),
                'description' => $faker->text(200),
            ]);
        }
    }
}
