<?php

namespace App\Http\Controllers\Api;

use App\Models\Deal;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DealsController extends ResourceController
{
    protected $model = Deal::class;

    protected $validateHandle = 'validateDeal';

    protected $shortAttributes = ['id', 'name'];

    protected $clearIdsOnDelete = [
        Task::class => 'related_id',
    ];

    protected $searchField = 'name';

    protected $searchFieldsLike = ['name', 'product', 'company', 'source'];

    public function setStage(Request $request, $id)
    {
        $record = $this->model::find($id);
        if(is_null($record)){
            return $this->notFoundResponse();
        } else {
            if(Auth::user()->hasFullRecordsAccess() || Auth::user()->isOwner($record)){
                $record->stage = $request->input('stage');
                $record->save();
                return $this->successResponse();
            } else {
                return $this->forbiddenResponse();
            }
        }
    }

    public function tasks($id)
    {
        return $this->successResponse([
            'tasks' => Task::where('related_module', 'Deal')->where('related_id', $id)->get(),
        ]);
    }
}
