<?php

namespace App\Http\Controllers\Api;

use App\Models\Contact;
use App\Models\Deal;
use App\Models\Lead;
use App\Models\Task;
use App\Traits\ResponseHelperTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    use ResponseHelperTrait;

    private function getRecent($model)
    {
        $records = $model::orderBy('created_at', 'desc')->limit(5);
        $user = Auth::user();
        if($user->hasFullRecordsAccess()){
            $records = $records->get();
        } else {
            $records = $records->where('user_id', $user->id)->get();
        }
        return $this->successResponse([
            'records' => $records
        ]);
    }

    public function recentLeads()
    {
        return $this->getRecent(Lead::class);
    }

    public function recentContacts()
    {
        return $this->getRecent(Contact::class);
    }

    public function recentDeals()
    {
        return $this->getRecent(Deal::class);
    }

    public function recentTasks()
    {
        return $this->getRecent(Task::class);
    }
}
