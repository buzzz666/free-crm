<?php

namespace App\Http\Controllers\Api;

use App\Traits\ResponseHelperTrait;
use App\Traits\Validate;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ResourceController extends Controller
{
    use ResponseHelperTrait;

    use Validate;

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @var string
     */
    protected $validateHandle;

    /**
     * @var array
     */
    protected $clearIdsOnDelete = [];

    /**
     * @var array
     */
    protected $shortAttributes = [];

    /**
     * @var string
     */
    protected $searchField = '';

    /**
     * @var array
     */
    protected $searchFieldsLike = [];

    /**
     * @var int
     */
    protected $perPage = 10;

    public function index(Request $request)
    {
        $builder = $this->model::query();
        !Auth::user()->hasFullRecordsAccess() && $builder->where('user_id', Auth::user()->id);
        $builder->orderBy($request->input('order_by', 'id'), $request->input('order_type', 'asc'));
        if($request->input('search')){
            $concat = '';
            foreach ($this->searchFieldsLike as $field){
                $concat .= ('ifnull(`' . $field . '`, \'\'), " "');
                $concat .= ', ';
            }
            $concat = rtrim($concat, ', " ", ');
            $builder->where(DB::raw('concat(' . $concat . ')'), 'like', DB::raw('\'%' . $request->input('search') . '%\''));
        }
        $paginated = $builder->paginate($this->perPage);
        return response()->json([
            'records' => $paginated
        ]);
    }

    public function store(Request $request)
    {
        $validator = $this->{$this->validateHandle}($request->input('data'));
        if($validator->fails()){
            return $this->badRequestResponse(['errors' => $validator->errors()->all()]);
        } else {
            $created = $this->model::create($request->input('data'));
            $show = true;
            if($created->user_id){
                $createdBySupervisor = User::find($created->user_id)->hasFullRecordsAccess();
                $isCurrentSupervisor = Auth::user()->hasFullRecordsAccess();
                if(!$isCurrentSupervisor && $createdBySupervisor){
                    $show = false;
                }
            }
            return $this->createdResponse([
                'created' => $created,
                'show' => $show
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = $this->{$this->validateHandle}($request->input('data'));
        if($validator->fails()){
            return $this->badRequestResponse(['errors' => $validator->errors()->all()]);
        } else {
            $updated = $this->model::find($id);
            if(Auth::user()->hasFullRecordsAccess() || Auth::user()->isOwner($updated)){
                $updated->update($request->input('data'));
                $show = true;
                if($updated->user_id){
                    $createdBySupervisor = User::find($updated->user_id)->hasFullRecordsAccess();
                    $isCurrentSupervisor = Auth::user()->hasFullRecordsAccess();
                    if(!$isCurrentSupervisor && $createdBySupervisor){
                        $show = false;
                    }
                }
                return $this->successResponse([
                    'updated' => $updated,
                    'show' => $show
                ]);
            } else {
                return $this->forbiddenResponse();
            }
        }
    }

    public function destroy($id)
    {
        $record = $this->model::find($id);
        if(is_null($record)){
            return $this->notFoundResponse();
        } else {
            if(Auth::user()->hasFullRecordsAccess() || Auth::user()->isOwner($record)){
                $record->delete();
                if(count($this->clearIdsOnDelete)){
                    foreach ($this->clearIdsOnDelete as $model => $field) {
                        $model::where($field, $id)->update([$field => null]);
                    }
                }
                return $this->successResponse();
            } else {
                return $this->forbiddenResponse();
            }
        }
    }

    public function show($id)
    {
        $record = $this->model::find($id);
        if(is_null($record)){
            return $this->notFoundResponse();
        } else {
            if(Auth::user()->hasFullRecordsAccess() || Auth::user()->isOwner($record)){
                return $this->successResponse([
                    'record' => $record
                ]);
            } else {
                return $this->forbiddenResponse();
            }
        }
    }

    public function short($id)
    {
        $record = $this->model::find($id);
        if(is_null($record)){
            return $this->notFoundResponse();
        } else {
            $response = [];
            foreach ($this->shortAttributes as $field) {
                $response[$field] = $record->{$field};
            }
            return $this->successResponse([
                'record' => $response
            ]);
        }
    }

    public function search(Request $request)
    {
        $search = '%' . $request->input('search') . '%';
        $records = $this->model::where($this->searchField, 'like', $search)->limit(20);
        $currentUser = Auth::user();
        if($currentUser->hasFullRecordsAccess()){
            $records = $records->get();
        } else {
            $records = $records->where('user_id', $currentUser->id)->get();
        }
        return $this->successResponse([
            'records' => $records
        ]);
    }
}
