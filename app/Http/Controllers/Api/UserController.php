<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\ResetMail;
use App\Traits\ResponseHelperTrait;
use App\Traits\Validate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserController extends Controller
{
    use Validate;

    use ResponseHelperTrait;

    public function login(Request $request)
    {
        // Validate
        $credentials = $request->only(['email', 'password']);
        $validator = $this->validateLogin($credentials);
        if($validator->fails()){
            return $this->badRequestResponse([
                'errors' => $validator->errors()
            ]);
        }
        // Attempt to auth
        if(Auth::attempt($credentials, true)){
            $user = Auth::user();
            if($user->active){
                return $this->successResponse([
                    'user' => array_merge($user->toArray(), ['api_token' => $user->api_token])
                ]);
            } else {
                return $this->badRequestResponse([
                    'errors' => [
                        'message' => ['This user has been deactivated.']
                    ]
                ]);
            }
        } else {
            return $this->unauthorizedResponse();
        }
    }

    public function user()
    {
        $user = Auth::user();
        return response()->json([
            'user' => array_merge($user->toArray(), ['api_token' => $user->api_token])
        ]);
    }

    public function index()
    {
        return $this->successResponse([
            'users' => User::all()
        ]);
    }

    public function changePassword(Request $request)
    {
        $credentials = $request->input('passwords');
        $validator = $this->validateChangePassword($credentials);
        if($validator->fails()){
            return $this->badRequestResponse([
                'errors' => $validator->errors()
            ]);
        } else {
            $user = Auth::user();
            if(Hash::check($credentials['password'], $user->password)){
                $user->password = Hash::make($credentials['passwordNew']);
                $user->save();
                return $this->successResponse();
            } else {
                return $this->badRequestResponse([
                    'errors' => ['Old password is not correct']
                ]);
            }
        }
    }

    public function changeName(Request $request)
    {
        $name = trim($request->input('name'));
        $user = Auth::user();
        $user->name = $name;
        $user->save();
        return $this->successResponse();
    }

    public function resetPassword(Request $request)
    {
        $validator = $this->validateReset(['email' => $request->input('email')]);
        if($validator->fails()){
            return $this->badRequestResponse([
                'errors' => $validator->errors()
            ]);
        } else {
            $user = User::where('email', $request->input('email'))->first();
            if($user){
                $password = Str::random();
                $user->password = bcrypt($password);
                $user->save();
                Mail::to($user)->queue(new ResetMail($user, $password));
            }
            return $this->successResponse();
        }
    }
}
