<?php

namespace App\Http\Controllers\Api;

use App\Models\Contact;
use App\Models\Lead;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LeadsController extends ResourceController
{
    protected $model = Lead::class;

    protected $validateHandle = 'validateLead';

    protected $shortAttributes = ['id', 'first_name', 'last_name'];

    protected $clearIdsOnDelete = [
        Task::class => 'related_id',
    ];

    protected $searchFieldsLike = ['first_name', 'last_name', 'phone', 'email', 'skype', 'mobile'];

    public function __construct()
    {
        $this->searchField = DB::raw('CONCAT(`first_name`, " ", `last_name`)');
    }

    public function convert($id)
    {
        $lead = Lead::find($id);
        $leadData = $lead->toArray();
        unset($leadData['lead_source']);
        unset($leadData['created_at']);
        unset($leadData['updated_at']);
        unset($leadData['deleted_at']);
        unset($leadData['owner']);
        unset($leadData['id']);
        $newContact = Contact::create($leadData);
        $lead->delete();
        Task::where('related_module', 'Lead')->where('related_id', $id)->update([
            'related_module' => 'Contact',
            'related_id' => $newContact->id
        ]);
        return $this->successResponse([
            'converted' => $newContact
        ]);
    }

    public function tasks($id)
    {
        return $this->successResponse([
            'tasks' => Task::where('related_module', 'Lead')->where('related_id', $id)->get(),
        ]);
    }
}
