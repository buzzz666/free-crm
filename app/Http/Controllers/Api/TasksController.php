<?php

namespace App\Http\Controllers\Api;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TasksController extends ResourceController
{
    protected $model = Task::class;

    protected $validateHandle = 'validateTask';

    protected $searchField = 'subject';

    protected $searchFieldsLike = ['subject'];
}
