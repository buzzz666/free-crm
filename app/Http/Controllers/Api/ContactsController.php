<?php

namespace App\Http\Controllers\Api;

use App\Models\Contact;
use App\Models\Deal;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactsController extends ResourceController
{
    protected $model = Contact::class;

    protected $validateHandle = 'validateContact';

    protected $clearIdsOnDelete = [
        Deal::class => 'contact_id',
        Task::class => 'related_id',
    ];

    protected $shortAttributes = ['id', 'first_name', 'last_name'];

    protected $searchFieldsLike = ['first_name', 'last_name', 'phone', 'email', 'skype', 'mobile'];

    public function __construct()
    {
        $this->searchField = DB::raw('CONCAT(`first_name`, " ", `last_name`)');
    }

    public function tasks($id)
    {
        return $this->successResponse([
            'tasks' => Task::where('related_module', 'Contact')->where('related_id', $id)->get(),
        ]);
    }

    public function deals($id)
    {
        return $this->successResponse([
            'deals' => Deal::where('contact_id', $id)->get(),
        ]);
    }
}
