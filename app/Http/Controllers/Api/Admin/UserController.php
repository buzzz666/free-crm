<?php

namespace App\Http\Controllers\Api\Admin;

use App\Mail\InviteMail;
use App\Models\User;
use App\Traits\ResponseHelperTrait;
use App\Traits\Validate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserController extends Controller
{
    use Validate;

    use ResponseHelperTrait;

    public function invite(Request $request)
    {
        $credentials = $request->input('user');
        $validator = $this->validateInvite($credentials);
        if($validator->fails()){
            return $this->badRequestResponse([
                'errors' => $validator->errors()->all()
            ]);
        } else {
            $password = Str::random();
            $created = User::create(array_merge($credentials, [
                'password' => bcrypt($password),
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
                'api_token' => Str::random(60),
            ]));

            Mail::to($created)->queue(new InviteMail($created, $password));

            return $this->createdResponse();
        }
    }

    public function users()
    {
        return $this->successResponse([
            'users' => User::all()
        ]);
    }

    public function activate($id)
    {
        User::where('id', $id)->update(['active' => 1]);
        return $this->successResponse();
    }

    public function deactivate($id)
    {
        if($id !== 1){
            User::where('id', $id)->update(['active' => 0]);
            return $this->successResponse();
        } else {
            return $this->badRequestResponse([
                'errors' => [
                    'You cannot deactivate superadmin.'
                ]
            ]);
        }
    }

    public function setRole($id, $roleId)
    {
        if($id !== 1){
            User::where('id', $id)->update(['role_id' => (int)$roleId]);
            return $this->successResponse();
        } else {
            return $this->badRequestResponse([
                'errors' => [
                    'You cannot change superadmin role.'
                ]
            ]);
        }
    }
}
