<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Role;
use App\Traits\ResponseHelperTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    use ResponseHelperTrait;

    public function index()
    {
        return $this->successResponse([
            'roles' => Role::all()
        ]);
    }
}
