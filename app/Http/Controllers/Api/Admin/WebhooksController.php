<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\ResourceController;
use App\Models\Webhook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebhooksController extends ResourceController
{
    protected $model = Webhook::class;

    protected $validateHandle = 'validateWebhook';

    protected $searchField = 'url';
}
