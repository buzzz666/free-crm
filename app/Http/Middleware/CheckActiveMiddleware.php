<?php

namespace App\Http\Middleware;

use App\Traits\ResponseHelperTrait;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckActiveMiddleware
{
    use ResponseHelperTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->active){
            return $next($request);
        } else {
            return $this->forbiddenResponse([
                'errors' => ['This user has been deactivated.']
            ]);
        }
    }
}
