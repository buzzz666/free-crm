<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    protected $task;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $task
     */
    public function __construct($user, $task)
    {
        $this->user = $user;
        $this->task = $task;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.task-notify', [
            'user' => $this->user,
            'task' => $this->task,
        ]);
    }
}
