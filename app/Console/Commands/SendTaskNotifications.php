<?php

namespace App\Console\Commands;

use App\Mail\TaskNotificationMail;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendTaskNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notifications for all tasks needed';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $time = Carbon::now()->format('Y-m-d H:i:00');
        $tasks = Task::where('notified', 0)->where('notification_time', $time)
            ->get();
        $ids = $tasks->pluck('id')->toArray();
        foreach ($tasks as $task){
            Mail::to($task->owner)->queue(new TaskNotificationMail($task->owner, $task));
        }
        Task::whereIn('id', $ids)->update(['notified' => 1]);
    }
}
