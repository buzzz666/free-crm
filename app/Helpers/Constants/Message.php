<?php

namespace App\Helpers\Constants;

class Message
{
    const OK = 'ok';
    const CREATED = 'created';
    const BAD_REQUEST = 'bad_request';
    const UNAUTHORIZED = 'unauthorized';
    const FORBIDDEN = 'forbidden';
    const NOT_FOUND = 'not_found';
    const SERVER_ERROR = 'server_error';
}
