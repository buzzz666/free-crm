<?php

namespace App\Observers;

use App\Jobs\System\InvokeWebhook;
use App\Models\Lead;
use App\Models\Webhook;

class LeadObserver
{
    /**
     * Handle the lead "created" event.
     *
     * @param Lead $lead
     * @return void
     */
    public function created(Lead $lead)
    {
        $webhooks = Webhook::where('module', 'Leads')->where('action', 'Created')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $lead)->onConnection('redis');
        }
    }

    /**
     * Handle the lead "updated" event.
     *
     * @param Lead $lead
     * @return void
     */
    public function updated(Lead $lead)
    {
        $webhooks = Webhook::where('module', 'Leads')->where('action', 'Updated')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $lead)->onConnection('redis');
        }
    }

    /**
     * Handle the lead "deleted" event.
     *
     * @param Lead $lead
     * @return void
     */
    public function deleted(Lead $lead)
    {
        $webhooks = Webhook::where('module', 'Leads')->where('action', 'Deleted')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $lead)->onConnection('redis');
        }
    }
}
