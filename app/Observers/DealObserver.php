<?php

namespace App\Observers;

use App\Jobs\System\InvokeWebhook;
use App\Models\Deal;
use App\Models\Webhook;

class DealObserver
{
    /**
     * Handle the deal "created" event.
     *
     * @param Deal $deal
     * @return void
     */
    public function created(Deal $deal)
    {
        $webhooks = Webhook::where('module', 'Deals')->where('action', 'Created')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $deal)->onConnection('redis');
        }
    }

    /**
     * Handle the deal "updated" event.
     *
     * @param Deal $deal
     * @return void
     */
    public function updated(Deal $deal)
    {
        $webhooks = Webhook::where('module', 'Deals')->where('action', 'Updated')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $deal)->onConnection('redis');
        }
    }

    /**
     * Handle the deal "deleted" event.
     *
     * @param Deal $deal
     * @return void
     */
    public function deleted(Deal $deal)
    {
        $webhooks = Webhook::where('module', 'Deals')->where('action', 'Deleted')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $deal)->onConnection('redis');
        }
    }
}
