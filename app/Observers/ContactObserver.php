<?php

namespace App\Observers;

use App\Jobs\System\InvokeWebhook;
use App\Models\Contact;
use App\Models\Webhook;

class ContactObserver
{
    /**
     * Handle the contact "created" event.
     *
     * @param Contact $contact
     * @return void
     */
    public function created(Contact $contact)
    {
        $webhooks = Webhook::where('module', 'Contacts')->where('action', 'Created')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $contact)->onConnection('redis');
        }
    }

    /**
     * Handle the contact "updated" event.
     *
     * @param Contact $contact
     * @return void
     */
    public function updated(Contact $contact)
    {
        $webhooks = Webhook::where('module', 'Contacts')->where('action', 'Updated')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $contact)->onConnection('redis');
        }
    }

    /**
     * Handle the contact "deleted" event.
     *
     * @param Contact $contact
     * @return void
     */
    public function deleted(Contact $contact)
    {
        $webhooks = Webhook::where('module', 'Contacts')->where('action', 'Deleted')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $contact)->onConnection('redis');
        }
    }
}
