<?php

namespace App\Observers;

use App\Jobs\System\InvokeWebhook;
use App\Models\Task;
use App\Models\Webhook;

class TaskObserver
{
    /**
     * Handle the task "created" event.
     *
     * @param  \App\Models\Task  $task
     * @return void
     */
    public function created(Task $task)
    {
        $webhooks = Webhook::where('module', 'Tasks')->where('action', 'Created')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $task)->onConnection('redis');
        }
    }

    /**
     * Handle the task "updated" event.
     *
     * @param  \App\Models\Task  $task
     * @return void
     */
    public function updated(Task $task)
    {
        $webhooks = Webhook::where('module', 'Tasks')->where('action', 'Updated')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $task)->onConnection('redis');
        }
    }

    /**
     * Handle the task "deleted" event.
     *
     * @param  \App\Models\Task  $task
     * @return void
     */
    public function deleted(Task $task)
    {
        $webhooks = Webhook::where('module', 'Tasks')->where('action', 'Deleted')->get();
        foreach($webhooks as $webhook){
            InvokeWebhook::dispatch($webhook, $task)->onConnection('redis');
        }
    }
}
