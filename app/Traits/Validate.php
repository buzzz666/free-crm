<?php

namespace App\Traits;

use App\Models\Role;
use Illuminate\Support\Facades\Validator;

trait Validate
{
    /**
     * @param $credentials
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateLogin($credentials)
    {
        return Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6'
        ]);
    }

    /**
     * @param $credentials
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateReset($credentials)
    {
        return Validator::make($credentials, [
            'email' => 'required|email',
        ]);
    }

    /**
     * @param $credentials
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateChangePassword($credentials)
    {
        return Validator::make($credentials, [
            'password' => 'required|string|min:6',
            'passwordConfirm' => 'required|same:password',
            'passwordNew' => 'required|min:6|alpha_dash'
        ]);
    }

    /**
     * @param $contact
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateContact($contact)
    {
        return Validator::make($contact, [
            'first_name' => 'required|string',
            'user_id' => 'required|integer',
            'last_name' => 'nullable|string',
            'phone' => 'nullable|string',
            'mobile' => 'nullable|string',
            'skype' => 'nullable|string',
            'email' => 'nullable|string|email',
            'other_email' => 'nullable|string|email',
            'website' => 'nullable|string',
            'street' => 'nullable|string',
            'city' => 'nullable|string',
            'state' => 'nullable|string',
            'zip' => 'nullable|string',
            'country' => 'nullable|string',
            'description' => 'nullable|string',
            'utm_campaign' => 'nullable|string',
            'utm_medium' => 'nullable|string',
            'utm_term' => 'nullable|string',
            'utm_content' => 'nullable|string',
            'utm_source' => 'nullable|string',
            'gclid' => 'nullable|string',
        ]);
    }

    /**
     * @param $lead
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateLead($lead)
    {
        return Validator::make($lead, [
            'first_name' => 'required|string',
            'user_id' => 'required|integer',
            'last_name' => 'nullable|string',
            'phone' => 'nullable|string',
            'mobile' => 'nullable|string',
            'skype' => 'nullable|string',
            'email' => 'nullable|string|email',
            'other_email' => 'nullable|string|email',
            'website' => 'nullable|string',
            'lead_source' => 'nullable|string',
            'street' => 'nullable|string',
            'city' => 'nullable|string',
            'state' => 'nullable|string',
            'zip' => 'nullable|string',
            'country' => 'nullable|string',
            'description' => 'nullable|string',
            'utm_campaign' => 'nullable|string',
            'utm_medium' => 'nullable|string',
            'utm_term' => 'nullable|string',
            'utm_content' => 'nullable|string',
            'utm_source' => 'nullable|string',
            'gclid' => 'nullable|string',
        ]);
    }

    /**
     * @param $deal
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateDeal($deal)
    {
        $stages = [
            'New',
            'First Contact',
            'Demo/Presentation',
            'Needs Analysis',
            'Proposal/Price Quote',
            'Negotiation/Review',
            'Contract',
            'Execution',
            'Closed Won',
            'Closed Lost',
        ];

        $priorities = [
            'Highest',
            'High',
            'Medium',
            'Low',
            'Lowest',
            'Highest',
        ];

        return Validator::make($deal, [
            'name' => 'required|string',
            'user_id' => 'required|integer',
            'contact_id' => 'nullable|integer',
            'stage' => 'required|string|in:' . implode(',', $stages),
            'amount' => 'nullable|numeric',
            'currency' => 'nullable|string|in:UAH,USD,EUR',
            'product' => 'nullable|string',
            'company' => 'nullable|string',
            'date' => 'required|date',
            'priority' => 'nullable|string|in:' . implode(',', $priorities),
            'source' => 'nullable|string',
            'description' => 'nullable|string',
        ]);
    }

    /**
     * @param $task
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateTask($task)
    {
        $statuses = [
            'New',
            'Started',
            'Finished',
            'Failed',
            'Declined',
        ];

        $modules = [
            'Lead',
            'Contact',
            'Deal',
        ];

        $priorities = [
            'Highest',
            'High',
            'Medium',
            'Low',
            'Lowest',
            'Highest',
        ];

        return Validator::make($task, [
            'subject' => 'required|string',
            'user_id' => 'required|integer',
            'related_module' => 'nullable|string|in:' . implode(',', $modules) . '|required_if:related_id,integer',
            'related_id' => 'nullable|integer',
            'status' => 'required|string|in:' . implode(',', $statuses),
            'priority' => 'required|string|in:' . implode(',', $priorities),
            'due_date' => 'required|date',
            'closed_time' => 'nullable|date_format:Y-m-d H:i:s',
            'notification_time' => 'nullable|date_format:Y-m-d H:i:s',
            'notified' => 'nullable|boolean',
            'description' => 'nullable|string',
        ]);
    }

    /**
     * @param $webhook
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateWebhook($webhook)
    {
        $modules = [
            'Leads',
            'Contacts',
            'Deals',
            'Tasks',
        ];

        $actions = [
            'Created',
            'Updated',
            'Deleted',
        ];

        return Validator::make($webhook, [
            'module' => 'required|string|in:' . implode(',', $modules),
            'action' => 'required|string|in:' . implode(',', $actions),
            'url' => 'required|string|url',
        ]);
    }

    /**
     * @param $credentials
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateInvite($credentials)
    {
        $roleIds = Role::all()->pluck('id')->toArray();

        return Validator::make($credentials, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'role_id' => 'required|integer|in:' . implode(',', $roleIds)
        ]);
    }
}
