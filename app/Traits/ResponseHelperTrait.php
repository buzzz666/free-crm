<?php

namespace App\Traits;

use App\Helpers\Constants\Status;
use App\Helpers\Constants\Message;

trait ResponseHelperTrait
{
    private function jsonResponse(array $data = [], $status = Status::SUCCESS, $message = Message::OK)
    {
        return response()->json([
            'status' => $message,
            'data' => $data,
        ], $status);
    }

    protected function successResponse(array $data = [])
    {
        return $this->jsonResponse($data, Status::SUCCESS, Message::OK);
    }

    protected function createdResponse(array $data = [])
    {
        return $this->jsonResponse($data, Status::CREATED, Message::CREATED);
    }

    protected function badRequestResponse(array $data = [])
    {
        return $this->jsonResponse($data, Status::BAD_REQUEST, Message::BAD_REQUEST);
    }

    protected function unauthorizedResponse(array $data = [])
    {
        return $this->jsonResponse($data, Status::UNAUTHORIZED, Message::UNAUTHORIZED);
    }

    protected function forbiddenResponse(array $data = [])
    {
        return $this->jsonResponse($data, Status::FORBIDDEN, Message::FORBIDDEN);
    }

    protected function notFoundResponse(array $data = [])
    {
        return $this->jsonResponse($data, Status::NOT_FOUND, Message::NOT_FOUND);
    }

    protected function errorResponse(array $data = [])
    {
        return $this->jsonResponse($data, Status::SERVER_ERROR, Message::SERVER_ERROR);
    }
}
