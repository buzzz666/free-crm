<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deal extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $with = [
        'owner',
        'contact'
    ];

    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function contact()
    {
        return $this->belongsTo('App\Models\Contact', 'contact_id', 'id');
    }
}
