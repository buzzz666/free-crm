<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $with = [
        'owner'
    ];

    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
