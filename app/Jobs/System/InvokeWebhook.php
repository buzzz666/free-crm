<?php

namespace App\Jobs\System;

use App\Models\Webhook;
use App\Models\WebhookLog;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class InvokeWebhook implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $webhook;

    protected $record;

    /**
     * Create a new job instance.
     *
     * @param Webhook $webhook
     * @param $record
     */
    public function __construct(Webhook $webhook, $record)
    {
        $this->webhook = $webhook;
        $this->record = $record;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $headers = ['Accept' => 'application/json'];
            $body = \Unirest\Request\Body::json($this->record->toArray());
            $response = \Unirest\Request::post($this->webhook->url, $headers, $body);
            if($response->code >= 200 && $response->code < 300){
                $status = 'Succeed';
            } else {
                $status = 'Failed';
            }
            WebhookLog::create([
                'webhook_id' => $this->webhook->id,
                'status' => $status,
                'response_code' => $response->code,
            ]);
        } catch(\Exception $e) {
            Log::error('Exception: line=' . $e->getLine() . ', trace=' . $e->getTraceAsString());
            WebhookLog::create([
                'webhook_id' => $this->webhook->id,
                'status' => 'Exception: ' . $e->getMessage(),
            ]);
        }
    }
}
