// Plugins indexes
import './plugins/vuetify'
import './plugins/moment'
import './plugins/loader'
import './plugins/datetimepicker'

// Import main modules
import Vue from 'vue'
import App from './App.vue'
import {router} from './services/router'

// Main app
export const app = new Vue({
  router,
  render: h => h(App),
  data(){
    return {
      currentPage: 'Calendar',
    }
  },
  created(){
    setTimeout(() => {
        app.$emit('change-page', this.$route.meta.name)
    }, 1000)
  }
}).$mount('#app')
