export const serialize = (obj) => {
    let str = [];
    for (let p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}

export const buildFilters = (pagination = {}, search) => {
    let filters = {}
    if(pagination.hasOwnProperty('page')){
        filters.page = pagination.page
    }
    if(pagination.hasOwnProperty('descending')){
        filters.order_type = pagination.descending ? 'desc' : 'asc'
    }
    if(pagination.hasOwnProperty('sortBy')){
        filters.order_by = pagination.sortBy
    }
    if(search !== null && search !== undefined && search !== ''){
        filters.search = search
    }
    return filters;
}
