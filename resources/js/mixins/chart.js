import ChartJS  from 'chart.js';
import chartTrendline from "chartjs-plugin-trendline";
ChartJS.plugins.register(chartTrendline);

export const charts = {
    methods: {
        createChart(chartId, chartData) {
            const ctx = document.getElementById(chartId);
            return new ChartJS(ctx, {
                type: chartData.type,
                data: chartData.data,
                options: chartData.options,
            });
        }
    }
}
