export const summaryData = (qt = 1, type = 'month', threshold = 50) => {
    let {labels, sums, mca} = dataSetCreator.getSummarySet(qt, type, threshold)
    return {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                { // one line graph
                    label: 'Revenue',
                    data: sums,
                    backgroundColor: [
                        'rgba(21,101,192,0.3)',
                    ],
                    borderColor: [
                        '#36495d',
                    ],
                    borderWidth: 3,
                    trendlineLinear: {
                        style: "rgba(255,105,180, 1)",
                        lineStyle: "solid",
                        width: 3
                    }
                },
                { // one line graph
                    label: 'SMA 3',
                    data: mca,
                    backgroundColor: [
                        'rgba(107,246,44,0)',
                    ],
                    borderColor: [
                        '#3ae227',
                    ],
                    borderWidth: 3,
                },
            ],
        },
        options: {
            responsive: true,
            aspectRatio: 3,
            lineTension: 1,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        padding: 25,
                    }
                }]
            }
        }
    }
}

const dataSetCreator = {
    getSummarySet(qt = 1, type = 'month', threshold = 50) {
        let labels = [];
        switch (type) {
            case 'month':
                labels = [
                    'January 2020',
                    'February 2020',
                    'March 2020',
                    'April 2020',
                    'May 2020',
                    'June 2020',
                    'July 2020',
                    'August 2020',
                    'September 2020',
                    'October 2020',
                    'November 2020',
                    'December 2020',
                    'January 2021',
                    'February 2021',
                ]
                break
            default:
                labels = ['2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021']
        }

        let sums = [];
        let base = 2000;
        for (const index in labels) {
            if (type === 'month') {
                if (index < 12) {
                    let num = this.getRandomInt(base, base + 3000) * qt / 100;
                    if (num < threshold) {
                        sums.push(0);
                    } else {
                        sums.push(num);
                    }
                } else {
                    sums.push(null);
                }
            }
            if (type === 'year') {
                if (index < 8) {
                    let num = this.getRandomInt(base, base + 3000) * qt / 100;
                    if (num < threshold) {
                        sums.push(0);
                    } else {
                        sums.push(num);
                    }
                } else {
                    sums.push(null);
                }
            }
            base += 200;
        }
        let mca = [];
        for (const index in sums) {
            let i = parseInt(index);
            if (sums[i - 1] !== undefined && sums[i - 1] !== null && sums[i + 1] !== undefined && sums[i + 1] !== null) {
                mca.push(Math.ceil((sums[i - 1] + sums[i] + sums[i + 1]) / 3));
            } else {
                mca.push(null);
            }
        }

        return {labels, sums, mca};
    },
    getRandomInt(min, max) {
        min = Math.ceil(min)
        max = Math.floor(max)
        return Math.floor(Math.random() * (max - min)) + min
    }
}
