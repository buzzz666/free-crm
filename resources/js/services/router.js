import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from "../pages/Dashboard"
import Leads from "../pages/Leads/Leads"
import NewLead from "../pages/Leads/NewLead"
import EditLead from "../pages/Leads/EditLead"
import ViewLead from "../pages/Leads/ViewLead"
import Contacts from "../pages/Contacts/Contacts"
import ViewContact from "../pages/Contacts/ViewContact"
import NewContact from "../pages/Contacts/NewContact"
import EditContact from "../pages/Contacts/EditContact"
import Deals from "../pages/Deals/Deals"
import Tasks from "../pages/Tasks/Tasks"
import AdminSettings from "../pages/AdminSettings"
import NewDeal from "../pages/Deals/NewDeal"
import EditDeal from "../pages/Deals/EditDeal"
import ViewDeal from "../pages/Deals/ViewDeal"
import {bus} from "./bus";
import NewTask from "../pages/Tasks/NewTask"
import EditTask from "../pages/Tasks/EditTask"
import ViewTask from "../pages/Tasks/ViewTask"
import AccountSettings from "../pages/AccountSettings"

Vue.use(VueRouter)

export const router = new VueRouter({
    routes: [
        {
            path: '/',
            component: Dashboard,
            meta: {
                name: 'Dashboard',
                admin: false
            }
        },
        {
            path: '/leads',
            component: Leads,
            meta: {
                name: 'Leads',
                admin: false
            }
        },
        {
            path: '/leads/edit/:id',
            component: EditLead,
            meta: {
                name: 'Edit Lead',
                admin: false
            }
        },
        {
            path: '/leads/view/:id',
            component: ViewLead,
            meta: {
                name: 'View Lead',
                admin: false
            }
        },
        {
            path: '/leads/new/',
            component: NewLead,
            meta: {
                name: 'New Lead',
                admin: false
            }
        },
        {
            path: '/contacts',
            component: Contacts,
            meta: {
                name: 'Contacts',
                admin: false
            }
        },
        {
            path: '/contacts/edit/:id',
            component: EditContact,
            meta: {
                name: 'Edit Contact',
                admin: false
            }
        },
        {
            path: '/contacts/view/:id',
            component: ViewContact,
            meta: {
                name: 'View Contact',
                admin: false
            }
        },
        {
            path: '/contacts/new/',
            component: NewContact,
            meta: {
                name: 'New Contact',
                admin: false
            }
        },
        {
            path: '/deals',
            component: Deals,
            meta: {
                name: 'Deals',
                admin: false
            }
        },
        {
            path: '/deals/edit/:id',
            component: EditDeal,
            meta: {
                name: 'Edit Deal',
                admin: false
            }
        },
        {
            path: '/deals/view/:id',
            component: ViewDeal,
            meta: {
                name: 'View Deal',
                admin: false
            }
        },
        {
            path: '/deals/new/',
            component: NewDeal,
            meta: {
                name: 'New Deal',
                admin: false
            }
        },
        {
            path: '/tasks/edit/:id',
            component: EditTask,
            meta: {
                name: 'Edit Task',
                admin: false
            }
        },
        {
            path: '/tasks/view/:id',
            component: ViewTask,
            meta: {
                name: 'View Task',
                admin: false
            }
        },
        {
            path: '/tasks/new/',
            component: NewTask,
            meta: {
                name: 'New Task',
                admin: false
            }
        },
        {
            path: '/tasks',
            component: Tasks,
            meta: {
                name: 'Tasks',
                admin: false
            }
        },
        {
            path: '/settings/admin',
            component: AdminSettings,
            meta: {
                name: 'Admin Settings',
                admin: true
            }
        },
        {
            path: '/settings/account',
            component: AccountSettings,
            meta: {
                name: 'Account Settings',
                admin: false
            }
        },
    ],
    isAdmin: false,
    isFullAccess: false
})

router.beforeEach((to, from, next) => {
    bus.$emit('change-page', to.meta.name)
    document.title = to.meta.name
    if(to.meta.admin){
        if(router.isAdmin){
            next()
        } else {
            router.push('/')
        }
    } else {
        next()
    }
})
