<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\UserController@login');
Route::post('user/reset-password', 'Api\UserController@resetPassword');

Route::group([
    'middleware' => ['auth:api', 'active'],
    'namespace' => 'Api'
], function(){
    Route::get('recent/leads',  'DashboardController@recentLeads');
    Route::get('recent/contacts',  'DashboardController@recentContacts');
    Route::get('recent/deals',  'DashboardController@recentDeals');
    Route::get('recent/tasks',  'DashboardController@recentTasks');

    Route::get('related/tasks/contact/{id}', 'ContactsController@tasks');
    Route::get('related/deals/contact/{id}', 'ContactsController@deals');
    Route::get('related/tasks/lead/{id}', 'LeadsController@tasks');
    Route::get('related/tasks/deal/{id}', 'DealsController@tasks');

    Route::get('user', 'UserController@user');
    Route::post('user/change-name', 'UserController@changeName');
    Route::post('user/change-password', 'UserController@changePassword');
    Route::get('users', 'UserController@index');

    Route::get('contacts/search', 'ContactsController@search')->name('contacts.search');
    Route::get('contacts/short/{id}', 'ContactsController@short')->name('contacts.short');
    Route::resource('contacts', 'ContactsController');

    Route::get('leads/search', 'LeadsController@search')->name('leads.search');
    Route::get('leads/short/{id}', 'LeadsController@short')->name('leads.short');
    Route::put('leads/convert/{id}', 'LeadsController@convert')->name('leads.convert');
    Route::resource('leads', 'LeadsController');

    Route::post('deals/stage/{id}', 'DealsController@setStage');
    Route::get('deals/search', 'DealsController@search')->name('deals.search');
    Route::get('deals/short/{id}', 'DealsController@short')->name('deals.short');
    Route::resource('deals', 'DealsController');

    Route::get('tasks/search', 'TasksController@search')->name('tasks.search');
    Route::resource('tasks', 'TasksController');

    Route::group([
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'admin'
    ], function(){
        Route::resource('webhooks', 'WebhooksController');
        Route::get('users', 'UserController@users');
        Route::post('users/invite', 'UserController@invite');
        Route::get('users/activate/{id}', 'UserController@activate');
        Route::get('users/deactivate/{id}', 'UserController@deactivate');
        Route::post('users/invite', 'UserController@invite');
        Route::get('users/role/{id}/{roleId}', 'UserController@setRole');
        Route::get('roles', 'RolesController@index');
    });
});
